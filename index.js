const fsOld = require('fs');
const fs = fsOld.promises;
const FormData = require('form-data');
const util = require('util');
const fetch = require('node-fetch');

const {tipuriFisier} = require('./nomen');

const {loginToken} =require('./auth');

const judet = "cv";
const maxNrElevi=100;

const elevURL = "http://burse.rpss.ro/api/burse/pg/elev/?judet="+judet+"&uid=&search=&ordering=nume&page=1&page_size="+maxNrElevi;

const uploadURL = "http://burse.rpss.ro/api/burse/fisier/";

(async ()=> {

const resultCSV = [];

//get elev list
const elevListResponse = await fetch(elevURL,{
  method: 'get',
  headers: { 'Authorization': 'Basic ' + loginToken }
});
const elevList = await elevListResponse.json();

console.log("Found: "+ elevList.results.length);

//for each elev
await elevList.results.reduce(
  async (lastPromise,elev)=>{
    await lastPromise;
    //for each tipFisier
    await tipuriFisier.reduce(
      async (lastPromise2,tipFisier)=>{
        await lastPromise2;

        //get filename for logging purposes
        const fileName = elev.cnp+"_"+tipFisier.name;

        //check if file is already uploaded
        if (elev.fisiere.find(fisier=>fisier.tip_fisier==tipFisier.id)){
          resultCSV.push([fileName,"exists"]);
          return Promise.resolve();
        } 
        
        //uploaded file not found, check on filesystem
        //check if file is readable
        let fileReadable = true;
        try {
          await fs.access("files/"+fileName+".pdf",fsOld.constants.R_OK);
        } catch(err){
          fileReadable = false;
          resultCSV.push([fileName,"unreadable"]);
        }
        
        if (fileReadable) {
          console.log("Uploading : "+ fileName);
          //upload found files
          const formData = new FormData();
          formData.append('id', '');
          formData.append('elev', elev.id);
          formData.append('tip_fisier', tipFisier.id);
          formData.append('fisier', fsOld.readFileSync("files/"+fileName+".pdf"), {
            contentType: 'application/pdf',
            filename: fileName+".pdf",
            filepath: "files/"+fileName+".pdf"
          });
          
          const contentLength = formData.getLengthSync();
          const uploadResult = await fetch(
            uploadURL,
            {
              method: 'post',
              headers: Object.assign({'Authorization': 'Basic ' + loginToken},formData.getHeaders()),
              body: formData
            }
          );
          resultCSV.push([fileName,"uploaded"]);
        }
      },
      Promise.resolve()
    );
    return Promise.resolve();
  },
  Promise.resolve());

  //write to log.csv
  let data = resultCSV.reduce( 
    (csv,line)=>{
      return csv + `${line[0]},${line[1]}\n`;
    },
    "\uFEFF" + "Filename,Status\n" //first line is UTF8 BOM for MSExcel and the header
    );
  await fs.writeFile("lastlog.csv",data);
})()
