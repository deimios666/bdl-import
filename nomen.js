module.exports = {
  tipuriFisier: [
    {
      id: 4,
      denumire: "T1 - cerere de inscriere",
      nume_fisier: "T1-cerere inscriere",
      inclus_in_export: true,
      name: "t1"
    },
    {
      id: 5,
      denumire: "T2 - CI sau CN elev",
      nume_fisier: "T2 - CI sau CN elev",
      inclus_in_export: true,
      name: "t2"
    },
    {
      id: 6,
      denumire: "T3 - CI sau CN membrii famile",
      nume_fisier: "T3-CI/CN membrii fam",
      inclus_in_export: true,
      name: "t3"
    },
    {
      id: 7,
      denumire: "T4 - Documente statut juridic sau medical",
      nume_fisier: "T4-Documente statut",
      inclus_in_export: true,
      name: "t4"
    },
    {
      id: 8,
      denumire: "T5 - Documente venituri famile",
      nume_fisier: "T5-documente venit",
      inclus_in_export: true,
      name: "t5"
    },
    {
      id: 9,
      denumire: "T6 - Adeverinta scolara",
      nume_fisier: "T6-Adeverinta scoala",
      inclus_in_export: true,
      name: "t6"
    },
    {
      id: 10,
      denumire: "T7 - Ancheta sociala",
      nume_fisier: "T7 - Ancheta sociala",
      inclus_in_export: true,
      name: "t7"
    }
  ]
};